<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;


/**
 * appprodUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appprodUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    static private $declaredRouteNames = array(
       'home_index' => true,
       'dictionary_index' => true,
       'dictionary_show_pages' => true,
       'dictionary_show_units' => true,
       'tests_index' => true,
       'tests_generate_test' => true,
       'tests_check_test' => true,
       'tests_show_hint' => true,
       'contact_index' => true,
       'contact_post' => true,
       'admin_index' => true,
       'admin_add_page' => true,
       'admin_add_page_post' => true,
       'admin_add_unit' => true,
       'admin_add_unit_post' => true,
       'admin_add_word' => true,
       'admin_add_word_post' => true,
       'admin_add_word_unitpost' => true,
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function generate($name, $parameters = array(), $absolute = false)
    {
        if (!isset(self::$declaredRouteNames[$name])) {
            throw new RouteNotFoundException(sprintf('Route "%s" does not exist.', $name));
        }

        $escapedName = str_replace('.', '__', $name);

        list($variables, $defaults, $requirements, $tokens) = $this->{'get'.$escapedName.'RouteInfo'}();

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $absolute);
    }

    private function gethome_indexRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\HomeController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/',  ),));
    }

    private function getdictionary_indexRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\DictionaryController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/dictionary',  ),));
    }

    private function getdictionary_show_pagesRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\DictionaryController::showPagesAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/dictionary/show-pages',  ),));
    }

    private function getdictionary_show_unitsRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\DictionaryController::showUnitsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/dictionary/show-units',  ),));
    }

    private function gettests_indexRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\TestsController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/tests',  ),));
    }

    private function gettests_generate_testRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\TestsController::generateTestAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/tests/generate-test',  ),));
    }

    private function gettests_check_testRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\TestsController::checkTestAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/tests/check-test',  ),));
    }

    private function gettests_show_hintRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\TestsController::showHintAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/tests/show-hint',  ),));
    }

    private function getcontact_indexRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\ContactController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/contact',  ),));
    }

    private function getcontact_postRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\ContactController::postAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/contact/post',  ),));
    }

    private function getadmin_indexRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin',  ),));
    }

    private function getadmin_add_pageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addPageAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/add-page',  ),));
    }

    private function getadmin_add_page_postRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addPagePostAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/add-page/post',  ),));
    }

    private function getadmin_add_unitRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addUnitAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/add-unit',  ),));
    }

    private function getadmin_add_unit_postRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addUnitPostAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/add-unit/post',  ),));
    }

    private function getadmin_add_wordRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addWordAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/add-word',  ),));
    }

    private function getadmin_add_word_postRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addWordPostAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/add-word/post',  ),));
    }

    private function getadmin_add_word_unitpostRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addWordUnitPostAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/add-word/unitpost',  ),));
    }
}
