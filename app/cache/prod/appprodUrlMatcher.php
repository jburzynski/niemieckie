<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appprodUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appprodUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = urldecode($pathinfo);

        // home_index
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home_index');
            }
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\HomeController::indexAction',  '_route' => 'home_index',);
        }

        // dictionary_index
        if ($pathinfo === '/dictionary') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\DictionaryController::indexAction',  '_route' => 'dictionary_index',);
        }

        // dictionary_show_pages
        if ($pathinfo === '/dictionary/show-pages') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\DictionaryController::showPagesAction',  '_route' => 'dictionary_show_pages',);
        }

        // dictionary_show_units
        if ($pathinfo === '/dictionary/show-units') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\DictionaryController::showUnitsAction',  '_route' => 'dictionary_show_units',);
        }

        // tests_index
        if ($pathinfo === '/tests') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\TestsController::indexAction',  '_route' => 'tests_index',);
        }

        // tests_generate_test
        if ($pathinfo === '/tests/generate-test') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\TestsController::generateTestAction',  '_route' => 'tests_generate_test',);
        }

        // tests_check_test
        if ($pathinfo === '/tests/check-test') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\TestsController::checkTestAction',  '_route' => 'tests_check_test',);
        }

        // tests_show_hint
        if ($pathinfo === '/tests/show-hint') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\TestsController::showHintAction',  '_route' => 'tests_show_hint',);
        }

        // contact_index
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\ContactController::indexAction',  '_route' => 'contact_index',);
        }

        // contact_post
        if ($pathinfo === '/contact/post') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\ContactController::postAction',  '_route' => 'contact_post',);
        }

        // admin_index
        if ($pathinfo === '/admin') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::indexAction',  '_route' => 'admin_index',);
        }

        // admin_add_page
        if ($pathinfo === '/admin/add-page') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addPageAction',  '_route' => 'admin_add_page',);
        }

        // admin_add_page_post
        if ($pathinfo === '/admin/add-page/post') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addPagePostAction',  '_route' => 'admin_add_page_post',);
        }

        // admin_add_unit
        if ($pathinfo === '/admin/add-unit') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addUnitAction',  '_route' => 'admin_add_unit',);
        }

        // admin_add_unit_post
        if ($pathinfo === '/admin/add-unit/post') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addUnitPostAction',  '_route' => 'admin_add_unit_post',);
        }

        // admin_add_word
        if ($pathinfo === '/admin/add-word') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addWordAction',  '_route' => 'admin_add_word',);
        }

        // admin_add_word_post
        if ($pathinfo === '/admin/add-word/post') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addWordPostAction',  '_route' => 'admin_add_word_post',);
        }

        // admin_add_word_unitpost
        if ($pathinfo === '/admin/add-word/unitpost') {
            return array (  '_controller' => 'Delfin\\WebBundle\\Controller\\AdminController::addWordUnitPostAction',  '_route' => 'admin_add_word_unitpost',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
