<?php

namespace Delfin\BusinessBundle;

class UUID
{
	public static function generateV4()
	{
		return sprintf("%04x%04x-%04x-%04x-%04x-%04x%04x%04x",
				mt_rand(0, 0xffff), mt_rand(0, 0xffff),
				mt_rand(0, 0xffff),
				mt_rand(0, 0x0fff) | 0x4000,
				mt_rand(0, 0x3fff) | 0x8000,
				mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
	}
        
        /**
         * @assert (0, 0) == 0
         * @assert (0, 1) == 1
         * @assert (1, 1) == 2
         */
        public static function add($a, $b)
        {
            return $a + $b;
        }
}