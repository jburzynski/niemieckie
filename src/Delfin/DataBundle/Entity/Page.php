<?php

namespace Delfin\DataBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Delfin\DataBundle\Entity\Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="Delfin\DataBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @var string $pageId
     *
     * @ORM\Column(name="page_id", type="string", length=36)
     * @ORM\Id
     */
    protected $pageId;

    /**
     * @var string $unitId
     *
     * @ORM\Column(name="unit_id", type="string", length=36)
     */
    protected $unitId;

    /**
     * @var integer $number
     *
     * @ORM\Column(name="number", type="integer")
     */
    protected $number;
    
    /**
     * @ORM\OneToMany(targetEntity="Word", mappedBy="page")
     * @ORM\OrderBy({"german" = "ASC"})
     */
    protected $words;
    
    /**
     * @ORM\ManyToOne(targetEntity="Unit", inversedBy="page")
     * @ORM\JoinColumn(name="unit_id", referencedColumnName="unit_id")
     */
    protected $unit;
    
    /**
     * Set pageId
     *
     * @param string $pageId
     */
    public function setPageId($pageId)
    {
    	$this->pageId = $pageId;
    }
    /**
     * Get pageId
     *
     * @return string
     */
    public function getPageId()
    {
    	return $this->pageId;
    }
    
    /**
     * Set unitId
     *
     * @param string $unitId
     */
    public function setUnitId($unitId)
    {
    	$this->unitId = $unitId;
    }
    /**
     * Get unitId
     *
     * @return string
     */
    public function getUnitId()
    {
    	return $this->unitId;
    }
    
    /**
     * Set number
     *
     * @param integer $number
     */
    public function setNumber($number)
    {
    	$this->number = $number;
    }
    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
    	return $this->number;
    }
    
    /**
     * Set words
     *
     * @param ArrayCollection $words
     */
    public function setWords($words)
    {
    	$this->words = $words;
    }
    /**
     * Get words
     *
     * @return ArrayCollection
     */
    public function getWords()
    {
    	return $this->words;
    }
    
    /**
     * Set unit
     *
     * @param Unit $unit
     */
    public function setUnit($unit)
    {
    	$this->unit = $unit;
    }
    /**
     * Get unit
     *
     * @return Unit
     */
    public function getUnit()
    {
    	return $this->unit;
    }
    
    public function __construct()
    {
    	$this->words = new ArrayCollection;
    }
}