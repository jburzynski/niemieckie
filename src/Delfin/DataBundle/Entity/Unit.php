<?php

namespace Delfin\DataBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Delfin\DataBundle\Entity\Unit
 *
 * @ORM\Table(name="unit")
 * @ORM\Entity(repositoryClass="Delfin\DataBundle\Repository\UnitRepository")
 */
class Unit
{
    /**
     * @var string $unitId
     *
     * @ORM\Column(name="unit_id", type="string", length=36)
     * @ORM\Id
     */
    protected $unitId;

    /**
     * @var integer $pages
     *
     * @ORM\Column(name="number", type="integer")
     */
    protected $number;
    
    /**
     * @var ArrayCollection $pages
     *
     * @ORM\OneToMany(targetEntity="Page", mappedBy="unit")
     * @ORM\OrderBy({"number" = "ASC"})
     */
    protected $pages;
    
    /**
     * Set unitId
     *
     * @param string $unit_id;
     */
    public function setUnitId($unitId)
    {
    	$this->unitId = $unitId;
    }
    /**
     * Get unitId
     *
     * @return string
     */
    public function getUnitId()
    {
    	return $this->unitId;
    }
    
    /**
     * Set number
     *
     * @param integer $number
     */
    public function setNumber($number)
    {
    	$this->number = $number;
    }
    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
    	return $this->number;
    }
    
    /**
     * Get pages
     *
     * @return ArrayCollection
     */
    public function getPages()
    {
    	return $this->pages;
    }
    
    public function __construct()
    {
    	$this->pages = new ArrayCollection;
    }
}