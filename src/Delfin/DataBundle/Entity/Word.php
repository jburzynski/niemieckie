<?php

namespace Delfin\DataBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Delfin\DataBundle\Entity\Word
 *
 * @ORM\Table(name="word")
 * @ORM\Entity(repositoryClass="Delfin\DataBundle\Repository\WordRepository")
 */
class Word
{
    /**
     * @var string $wordId
     *
     * @ORM\Column(name="word_id", type="string", length=36)
     * @ORM\Id
     */
    protected $wordId;

    /**
     * @var string $pageId
     *
     * @ORM\Column(name="page_id", type="string", length=36)
     */
    protected $pageId;

    /**
     * @var string $polish
     *
     * @ORM\Column(name="polish", type="string", length=255)
     */
    protected $polish;

    /**
     * @var string $german
     *
     * @ORM\Column(name="german", type="string", length=255)
     */
    protected $german;

    /**
     * @var string $article
     *
     * @ORM\Column(name="article", type="string", length=1)
     */
    protected $article;

    /**
     * @var string $plural
     *
     * @ORM\Column(name="plural", type="string", length=5)
     */
    protected $plural;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=31)
     */
    protected $type;
    
    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="word")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="page_id")
     */
    protected $page;

    /**
     * Set wordId
     *
     * @param string $wordId
     */
    public function setWordId($wordId)
    {
    	$this->wordId = $wordId;
    }
    /**
     * Get wordId
     *
     * @return string 
     */
    public function getWordId()
    {
        return $this->wordId;
    }

    /**
     * Set pageId
     *
     * @param string $pageId
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
    }

    /**
     * Get pageId
     *
     * @return string 
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * Set polish
     *
     * @param string $polish
     */
    public function setPolish($polish)
    {
        $this->polish = $polish;
    }

    /**
     * Get polish
     *
     * @return string 
     */
    public function getPolish()
    {
        return $this->polish;
    }

    /**
     * Set german
     *
     * @param string $german
     */
    public function setGerman($german)
    {
        $this->german = $german;
    }

    /**
     * Get german
     *
     * @return string 
     */
    public function getGerman()
    {
        return $this->german;
    }

    /**
     * Set article
     *
     * @param string $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }

    /**
     * Get article
     *
     * @return string 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set plural
     *
     * @param string $plural
     */
    public function setPlural($plural)
    {
        $this->plural = $plural;
    }

    /**
     * Get plural
     *
     * @return string 
     */
    public function getPlural()
    {
        return $this->plural;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set page
     *
     * @param Page $page
     */
    public function setPage($page)
    {
    	$this->page = $page;
    }
    /**
     * Get page
     *
     * @return Page
     */
    public function getPage()
    {
    	return $this->page;
    }
}