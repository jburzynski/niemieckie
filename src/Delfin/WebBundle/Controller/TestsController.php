<?php

namespace Delfin\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Delfin\DataBundle\Entity\Word;
use Delfin\WebBundle\Form\Type\TestPagesType;

class TestsController extends Controller
{
	public function indexAction()
	{
		$testPagesForm = $this->createForm(new TestPagesType);
		
		return $this->render('DelfinWebBundle:Tests:index.html.twig', array('form' => $testPagesForm->createView()));
	}
	
	public function generateTestAction(Request $request)
	{
		$entityManager = $this->getDoctrine()->getEntityManager();
		$pagesQuery = $entityManager->createQuery("SELECT p FROM Delfin\DataBundle\Entity\Page p WHERE p.number BETWEEN ?1 AND ?2");
		$pagesQuery->setParameter(1, $request->get('pageFrom'));
		$pagesQuery->setParameter(2, $request->get('pageTo'));
		$pages = $pagesQuery->getResult();
		
		$words = array();
		foreach ($pages as $page)
		{
			foreach ($page->getWords() as $word)
			{
				$words[] = $word;
			}
		}
		shuffle($words);
		
		$testWords = array_slice($words, 0, 15);
		
		return $this->render('DelfinWebBundle:Tests:generate-test.html.twig', array('testWords' => $testWords));
	}
	
	public function checkTestAction(Request $request)
	{
		$words = $request->get('words');
		$wordRepository = $this->getDoctrine()->getEntityManager()->getRepository('Delfin\DataBundle\Entity\Word');
		$checkTable = array();
        $totalCount = 0;
        $correctCount = 0;
		
		foreach ($words as $id => $word)
		{
			$correct = true;
			
			$originalWord = $wordRepository->find($id);
			
			if ($word['german'] != $originalWord->getGerman())
			{
				$correct = false;
			}
			
			$typed = '';
			
			if (isset($word['article']))
			{
				if ($word['article'] != $originalWord->getArticle())
				{
					$correct = false;
				}
				
                if ($originalWord->getPlural() != NULL || $word['plural'] != '')
                {
                    $plural = $word['plural'];
                    $plural = substr($plural, 0, 1) == '-' || substr($plural, 0, 1) == ':' ? substr($plural, 1, strlen($plural) - 1) : $plural;
                    $plural = $word['plural-umlaut'] == 'true' ? ':'. $plural : '-'. $plural;

                    if ($plural != $originalWord->getPlural())
                    {
                        $correct = false;
                    }
                }
				
				$typed = '<strong>'. $word['article'] .'</strong> ';
			}
			
			$typed .= $word['german'];
			
			if (isset($word['article']) && $word['countable'] == 'true')
			{
				$typed .= ', '. $plural;
			}
			
			$checkTable[$id] = array('typed' => $typed);
            $checkTable[$id]['polish'] = $originalWord->getPolish();
                        
			if (!$correct)
			{
				$correctWord = '';
				if ($originalWord->getType() == 'noun')
				{
					$correctWord = '<strong>'. $originalWord->getArticle() .'</strong> ';
				}
				
				$correctWord .= $originalWord->getGerman();
				
				if ($originalWord->getType() == 'noun' && $originalWord->getPlural() != '')
				{
					$correctWord .= ', '. $originalWord->getPlural();
				}
				
				$checkTable[$id]['correct'] = $correctWord;
			}
                        
                        $totalCount++;
                        if ($correct)
                        {
                            $correctCount++;
                        }
		}
		
		//var_export($checkTable);
                
		return $this->render('DelfinWebBundle:Tests:check-test.html.twig', array('checkTable' => $checkTable, 'totalCount' => $totalCount, 'correctCount' => $correctCount));
	}
        
        public function showHintAction(Request $request)
        {
			mb_internal_encoding("UTF-8");
			
            $typed = $request->get('typed');
            $wordId = $request->get('wordId');
            $responseText = '';
            
            $wordRepository = $this->getDoctrine()->getEntityManager()->getRepository('Delfin\DataBundle\Entity\Word');
            $german = $wordRepository->find($wordId)->getGerman();

            $responseText = mb_substr($german, 0, mb_strlen($typed)) == $typed ? (mb_strlen($typed) < mb_strlen($german) ? mb_substr($german, 0, mb_strlen($typed) + 1) : '[[correct]]') : '[[incorrect]]';

            return new Response($responseText);
        }
}