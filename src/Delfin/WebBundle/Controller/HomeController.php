<?php

namespace Delfin\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('DelfinWebBundle:Home:index.html.twig');
    }
}
