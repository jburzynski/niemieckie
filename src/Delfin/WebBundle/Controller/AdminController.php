<?php

namespace Delfin\WebBundle\Controller;

use Delfin\BusinessBundle\UUID;
use Delfin\DataBundle\Entity\Unit;
use Delfin\DataBundle\Entity\Page;
use Delfin\DataBundle\Entity\Word;
use Delfin\WebBundle\Entity\Admin\AddPage;
use Delfin\WebBundle\Entity\Admin\AddUnit;
use Delfin\WebBundle\Entity\Admin\AddWord;
use Delfin\WebBundle\Entity\Admin\AddWordPage;
use Delfin\WebBundle\Form\Type\AddUnitType;
use Delfin\WebBundle\Form\Type\AddWordType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
	public function indexAction()
	{
		return $this->addWordAction();
	}
	
	public function addPageAction()
	{
		$entityManager = $this->getDoctrine()->getEntityManager();
		$units = $entityManager->getRepository('Delfin\DataBundle\Entity\Unit')->findAll();
		$choices = array();
		foreach ($units as $unit)
		{
			$choices[$unit->getUnitId()] = $unit->getNumber();
		}
		
		asort($choices);
		
		$addPageEntity = new AddPage;
		$addPageForm = $this->createFormBuilder($addPageEntity)
			->add('unitId', 'choice', array(
					'choices' => $choices,
					'multiple' => false,
					'expanded' => false)
			)
			->add('pageNumber', 'text')
			->getForm();
		
		return $this->render('DelfinWebBundle:Admin:add-page.html.twig', array('form' => $addPageForm->createView()));
	}
	
	public function addPagePostAction(Request $request)
	{
		$message = 'Błąd w dodawaniu strony';
		if ($request->getMethod() == 'POST')
		{
			$pageEntity = new Page;
			$pageEntity->setPageId(UUID::generateV4());
			
			$pageEntity->setNumber($request->get('pageNumber'));
				
			$entityManager = $this->getDoctrine()->getEntityManager();
			$pageEntity->setUnit($entityManager->getRepository('Delfin\DataBundle\Entity\Unit')->find($request->get('unitId')));
			$entityManager->persist($pageEntity);
			$entityManager->flush();
				
			$message = 'Dodano stronę';
		}
		
		return $this->render('DelfinWebBundle:Admin:add-page-post.html.twig', array('message' => $message));
	}
	
	public function addUnitAction()
	{
		$addUnitEntity = new AddUnit;
		$addUnitForm = $this->createForm(new AddUnitType, $addUnitEntity);
		
		return $this->render('DelfinWebBundle:Admin:add-unit.html.twig', array('form' => $addUnitForm->createView()));
	}
	
	public function addUnitPostAction(Request $request)
	{
		$message = 'Błąd w dodawaniu rozdziału';
		if ($request->getMethod() == 'POST')
		{
			$unitEntity = new Unit;
			$unitEntity->setUnitId(UUID::generateV4());
			$unitEntity->setNumber($request->get('unitNumber'));
				
			$entityManager = $this->getDoctrine()->getEntityManager();
			$entityManager->persist($unitEntity);
			$entityManager->flush();
				
			$message = 'Dodano rozdział';
		}
		
		return $this->render('DelfinWebBundle:Admin:add-unit-post.html.twig', array('message' => $message));
	}
	
	public function addWordAction()
	{
//		$wordEntity = new Word;
//		$wordEntity->setWordId(UUID::generateV4());
//		$wordEntity->setType('other');
//		$wordEntity->setPolish('ąść');
//		$wordEntity->setGerman('asdf');
//			
//		$entityManager = $this->getDoctrine()->getEntityManager();
//		$wordEntity->setPage($entityManager->getRepository('Delfin\DataBundle\Entity\Page')->find('050b5f85-113e-4423-834c-c1719aa89256'));
//		$entityManager->persist($wordEntity);
//		$entityManager->flush();
		
		
		/////////////////////////////////
		
		
		$entityManager = $this->getDoctrine()->getEntityManager();
		$units = $entityManager->getRepository('Delfin\DataBundle\Entity\Unit')->findAll();
		$choices = array();
		foreach ($units as $unit)
		{
			$choices[$unit->getUnitId()] = $unit->getNumber();
		}
		
		asort($choices);
		
		$addWordEntity = new AddWord;
		$addWordForm = $this->createFormBuilder($addWordEntity)
			->add('unitId', 'choice', array(
					'choices' => $choices,
					'multiple' => false,
					'expanded' => false))
			->add('type', 'choice', array(
					'choices' => array(
							'noun' => 'rzeczownik',
							'verb' => 'czasownik',
							'adjective' => 'przymiotnik',
							'adverb' => 'przysłówek',
							'sentence' => 'zdanie',
							'other' => 'inne'),
					'multiple' => false,
					'expanded' => false))
			->add('polish', 'text')
			->add('german', 'text')
			->add('article', 'choice', array(
					'choices' => array(
							'r' => 'der',
							'e' => 'die',
							's' => 'das'),
					'multiple' => false,
					'expanded' => false))
			->add('plural', 'text')
			->getForm();
	
		return $this->render('DelfinWebBundle:Admin:add-word.html.twig', array('form' => $addWordForm->createView()));
	}
	
	public function addWordPostAction(Request $request)
	{
		$message = 'Błąd w dodawaniu słówka';
		if ($request->getMethod() == 'POST')
		{
			$wordEntity = new Word;
			$wordEntity->setWordId(UUID::generateV4());
			$wordEntity->setType($request->get('type'));
			$wordEntity->setPolish($request->get('polish'));
			$wordEntity->setGerman($request->get('german'));
			if ($wordEntity->getType() == 'noun')
			{
				$wordEntity->setArticle($request->get('article'));
				$wordEntity->setPlural($request->get('plural'));
			}
			
			$entityManager = $this->getDoctrine()->getEntityManager();
			$wordEntity->setPage($entityManager->getRepository('Delfin\DataBundle\Entity\Page')->find($request->get('pageId')));
			$entityManager->persist($wordEntity);
			$entityManager->flush();
			
			$message = 'Dodano słówko';
		}
	
		return $this->render('DelfinWebBundle:Admin:add-word-post.html.twig', array('message' => $message));
	}
	
	public function addWordUnitPostAction(Request $request)
	{
		$unit = $this->getDoctrine()->getRepository('Delfin\DataBundle\Entity\Unit')->find($request->get('unitId'));
		$pages = $unit->getPages()->toArray();
		$choices = array();
		foreach ($pages as $page)
		{
			$choices[$page->getPageId()] = $page->getNumber();
		}
		
		asort($choices);
		
		$addWordPageEntity = new AddWordPage;
		$addWordPageForm = $this->createFormBuilder($addWordPageEntity)
			->add('pageId', 'choice', array(
					'choices' => $choices,
					'multiple' => false,
					'expanded' => false))
			->getForm();
		
		return $this->render('DelfinWebBundle:Admin:add-word-unitpost.html.twig', array('form' => $addWordPageForm->createView()));
	}
}