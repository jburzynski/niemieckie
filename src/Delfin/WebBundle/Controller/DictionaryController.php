<?php

namespace Delfin\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Delfin\DataBundle\Entity\Word;
use Delfin\WebBundle\Entity\ShowPages;
use Delfin\WebBundle\Entity\ShowUnits;
use Delfin\WebBundle\Form\Type\ShowPagesType;
use Delfin\WebBundle\Form\Type\ShowUnitsType;

class DictionaryController extends Controller
{
	public function indexAction()
	{
		//$entityManager = $this->getDoctrine()->getEntityManager();
		//$wordRepository = $entityManager->getRepository('Delfin\DataBundle\Entity\Word');
		//$words = $wordRepository->findAll();
		$showPagesForm = $this->createForm(new ShowPagesType);
		$showUnitsForm = $this->createForm(new ShowUnitsType);
		return $this->render('DelfinWebBundle:Dictionary:index.html.twig', array(
				'showPagesForm' => $showPagesForm->createView(),
				'showUnitsForm' => $showUnitsForm->createView()
				));
	}
	
	public function showPagesAction(Request $request)
	{
		$entityManager = $this->getDoctrine()->getEntityManager();
		$pagesQuery = $entityManager->createQuery('SELECT p FROM Delfin\DataBundle\Entity\Page p WHERE p.number BETWEEN ?1 AND ?2 ORDER BY p.number ASC');
		$pagesQuery->setParameter(1, $request->get('pageFrom'));
		$pagesQuery->setParameter(2, $request->get('pageTo'));
		$pages = $pagesQuery->getResult();
		
		if (count($pages) > 0)
		{
			$recordCount = 0;
			foreach ($pages as $page)
			{
				$recordCount++;
				foreach ($page->getWords() as $word)
				{
					$recordCount++;
				}
			}
			
			$wordsPerColumn = ceil($recordCount / 3);
			
			$columnIndex = 0;
			$i = 0;
			$columns = array();
			foreach ($pages as $page)
			{
				if ($i == $wordsPerColumn)
				{
					$columnIndex++;
					$i = 0;
				}
				
				$columns[$columnIndex][$i]['german'] = 'Seite '. $page->getNumber();
				$columns[$columnIndex][$i]['polish'] = '';
				$i++;
				
				foreach ($page->getWords() as $word)
				{
					if ($i == $wordsPerColumn)
					{
						$columnIndex++;
						$i = 0;
					}
					
					$columns[$columnIndex][$i]['polish'] = $word->getPolish();
					
					$german = '';
					$type = $word->getType();
					if ($type == 'noun')
					{
						$german .= '<strong>'. $word->getArticle()  .'</strong>';
					}
					
					$german .= ' '. $word->getGerman();
					
					if ($type == 'noun' && $word->getPlural() != '')
					{
						$german .= ', '. $word->getPlural();
					}
					
					$columns[$columnIndex][$i]['german'] = $german;
					
					$i++;
				}
			}
		}
		
		return $this->render('DelfinWebBundle:Dictionary:show-pages.html.twig', array('columns' => $columns));
	}
	
	public function showUnitsAction(Request $request)
	{
		$entityManager = $this->getDoctrine()->getEntityManager();
		$unitsQuery = $entityManager->createQuery('SELECT u FROM Delfin\DataBundle\Entity\Unit u WHERE u.number BETWEEN ?1 AND ?2 ORDER BY u.number ASC');
		$unitsQuery->setParameter(1, $request->get('unitFrom'));
		$unitsQuery->setParameter(2, $request->get('unitTo'));
		$units = $unitsQuery->getResult();
		
		if (count($units) > 0)
		{
			$recordCount = 0;
			foreach ($units as $unit)
			{
				foreach ($unit->getPages() as $page)
				{
					$recordCount++;
					foreach ($page->getWords() as $word)
					{
						$recordCount++;
					}
				}
			}
			
			$wordsPerColumn = ceil($recordCount / 3);
			
			$columnIndex = 0;
			$i = 0;
			$columns = array();
			foreach ($units as $unit)
			{
				foreach ($unit->getPages() as $page)
				{
					if ($i == $wordsPerColumn)
					{
						$columnIndex++;
						$i = 0;
					}
					
					$columns[$columnIndex][$i]['german'] = 'Seite '. $page->getNumber();
					$columns[$columnIndex][$i]['polish'] = '';
					$i++;
					
					foreach ($page->getWords() as $word)
					{
						if ($i == $wordsPerColumn)
						{
							$columnIndex++;
							$i = 0;
						}
						
						$columns[$columnIndex][$i]['polish'] = $word->getPolish();
						
						$german = '';
						$type = $word->getType();
						if ($type == 'noun')
						{
							$german .= '<strong>'. $word->getArticle()  .'</strong>';
						}
						
						$german .= ' '. $word->getGerman();
						
						if ($type == 'noun' && $word->getPlural() != '')
						{
							$german .= ', '. $word->getPlural();
						}
						
						$columns[$columnIndex][$i]['german'] = $german;
						
						$i++;
					}
				}
			}
		}
		
		return $this->render('DelfinWebBundle:Dictionary:show-pages.html.twig', array('columns' => $columns));
	}
}