<?php

namespace Delfin\WebBundle\Controller;

use Delfin\WebBundle\Form\Type\ContactType;
use Delfin\WebBundle\Entity\Contact\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{
    public function indexAction()
    {
        $contactForm = $this->createForm(new ContactType);
        
        return $this->render('DelfinWebBundle:Contact:index.html.twig', array('form' => $contactForm->createView()));
    }
    
    public function postAction(Request $request)
    {
        $contactEntity = new Contact;
        $contactForm = $this->createForm(new ContactType, $contactEntity);
        
        $valid = false;
        
        if ($request->getMethod() == 'POST')
        {
            $contactForm->bindRequest($request);
            if ($contactForm->isValid())
            {
                $valid = true;
                
                $transport = \Swift_SmtpTransport::newInstance('mail.s4.laohost.net', 587, 'tls')
                    ->setUsername('contact@niemieckie.net')
                    ->setPassword('Sukienkamini61');
                
                $mailer = \Swift_Mailer::newInstance($transport);
                
                $message = \Swift_Message::newInstance('Wiadomość z niemieckie.net.')
                        ->setFrom(array($contactEntity->getEmail() => $contactEntity->getName()))
                        ->setTo(array('contact@niemieckie.net'))
                        ->setBody($contactEntity->getText() . "\r\n\r\nE-mail zwrotny: " . $contactEntity->getEmail());
                
                $result = $mailer->send($message);
            }
        }
        
        return $this->render('DelfinWebBundle:Contact:index.html.twig', array('form' => $contactForm->createView(), 'valid' => $valid));
    }
}