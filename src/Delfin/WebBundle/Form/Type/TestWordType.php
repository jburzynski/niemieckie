<?php

namespace Delfin\WebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class TestWordType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder->add('article', 'choice', array(
				'choices' => array(
						'r' => 'der',
						'e' => 'die',
						's' => 'das'),
				'expanded' => false,
				'multiple' => false));
		$builder->add('german', 'text');
		$builder->add('plural', 'text');
	}
	
	public function getName()
	{
		return 'TestWord';
	}
	
	public function getDefaultOptions(array $options)
	{
		return array('data_class' => 'Delfin\WebBundle\Entity\Tests\TestWord');
	}
}