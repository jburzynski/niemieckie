<?php

namespace Delfin\WebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('name', 'text');
        $builder->add('email', 'text');
        $builder->add('text', 'textarea');
    }
    
    public function getDefaultOptions(array $options)
    {
        return array('data_class' => 'Delfin\WebBundle\Entity\Contact\Contact');
    }
    
    public function getName()
    {
        return 'Contact';
    }
}
