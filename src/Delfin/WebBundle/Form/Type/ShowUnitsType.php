<?php

namespace Delfin\WebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ShowUnitsType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder->add('unitFrom', 'text');
		$builder->add('unitTo', 'text');
	}
	
	public function getName()
	{
		return 'showUnits';
	}
	
	public function getDefaultOptions(array $options)
	{
		return array('data_class' => 'Delfin\WebBundle\Entity\ShowUnits');
	}
}