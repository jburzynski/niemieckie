<?php

namespace Delfin\WebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ShowPagesType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder->add('pageFrom', 'text');
		$builder->add('pageTo', 'text');
	}
	
	public function getName()
	{
		return 'showPages';
	}
	
	public function getDefaultOptions(array $options)
	{
		return array('data_class' => 'Delfin\WebBundle\Entity\ShowPages');
	}
}