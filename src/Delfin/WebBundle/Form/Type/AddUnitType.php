<?php

namespace Delfin\WebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class AddUnitType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder->add('unitNumber', 'text');
	}
	
	public function getName()
	{
		return 'addUnit';
	}
	
	public function getDefaultOptions(array $options)
	{
		return array('data_class' => 'Delfin\WebBundle\Entity\Admin\AddUnit');
	}
}