<?php

namespace Delfin\WebBundle\Entity\Admin;

class AddWordPage
{
	protected $pageId;
	public function getPageId()
	{
		return $this->pageId;
	}
	
	public function setPageId($pageId)
	{
		$this->pageId = $pageId;
	}
}