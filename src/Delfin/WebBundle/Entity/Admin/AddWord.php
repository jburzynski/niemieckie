<?php

namespace Delfin\WebBundle\Entity\Admin;

class AddWord
{
	protected $unitId;
	public function getUnitId()
	{
		return $this->unitId;
	}
	
	public function setUnitId($unitId)
	{
		$this->unitId = $unitId;
	}
	
	protected $type;
	public function getType()
	{
		return $this->type;
	}
	
	public function setType($type)
	{
		$this->type = $type;
	}
	
	protected $polish;
	public function getPolish()
	{
		return $this->polish;
	}
	
	public function setPolish($polish)
	{
		$this->polish = $polish;
	}
	
	protected $german;
	public function getGerman()
	{
		return $this->german;
	}
	
	public function setGerman($german)
	{
		$this->german = $german;
	}
	
	protected $article;
	public function getArticle()
	{
		return $this->article;
	}
	
	public function setArticle($article)
	{
		$this->article = $article;
	}
	
	protected $plural;
	public function getPlural()
	{
		return $this->plural;
	}
	
	public function setPlural($plural)
	{
		$this->plural = $plural;
	}
}