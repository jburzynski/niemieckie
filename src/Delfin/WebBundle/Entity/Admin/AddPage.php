<?php

namespace Delfin\WebBundle\Entity\Admin;

class AddPage
{
	protected $unitId;
	protected $pageNumber;
	
	public function getUnitId()
	{
		return $this->unitId;
	}
	public function setUnitId($unitId)
	{
		$this->unitId = $unitId;
	}
	
	public function getPageNumber()
	{
		return $this->pageNumber;
	}
	public function setPageNumber($pageNumber)
	{
		$this->pageNumber = $pageNumber;
	}
}