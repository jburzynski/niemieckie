<?php

namespace Delfin\WebBundle\Entity\Admin;

class AddUnit
{
	protected $unitNumber;
	public function getUnitNumber()
	{
		return $this->unitNumber;
	}
	public function setUnitNumber($unitNumber)
	{
		$this->unitNumber = $unitNumber;
	}
}