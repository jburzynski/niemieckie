<?php

namespace Delfin\WebBundle\Entity;

class TestWord
{
	protected $german;
	public function setGerman($german)
	{
		$this->german = $german;
	}
	public function getGerman()
	{
		return $this->german;
	}
	
	protected $article;
	public function setArticle($article)
	{
		$this->article = $article;
	}
	public function getArticle()
	{
		return $this->article;
	}
	
	protected $plural;
	public function setPlural($plural)
	{
		$this->plural = $plural;
	}
	public function getPlural()
	{
		return $this->plural;
	}
}