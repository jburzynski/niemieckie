<?php

namespace Delfin\WebBundle\Entity\Dictionary;

class ShowPages
{
	protected $pageFrom;
	public function getPageFrom()
	{
		return $this->pageFrom;
	}
	
	protected $pageTo;
	public function getPageTo()
	{
		return $this->pageTo;
	}
}