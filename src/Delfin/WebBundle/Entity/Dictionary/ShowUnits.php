<?php

namespace Delfin\WebBundle\Entity\Dictionary;

class ShowUnits
{
	protected $unitFrom;
	public function getUnitFrom()
	{
		return $this->unitFrom;
	}
	
	protected $unitTo;
	public function getUnitTo()
	{
		return $this->unitTo;
	}
}